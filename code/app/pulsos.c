/*
 * pulsos.c
 *
 *  Created on: Apr 29, 2017
 *      Author: matt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

#define CANAL_H 0
#define CANAL_V 1
#define COMP_I 0
#define COMP_Q 1

#define CANT_GATE 500 //250/0.5
#define CANT_PULSOS 72

#define FILE_DIR "/media/sf_Google_Drive/Facultad/quinto/sistemas_operativos_II/openmp_tp2_so2/code/pulsos.iq"
//#define FILE_DIR "pulsos.iq"

//typedef float Mega_Matriz[2][2][CANT_GATE][CANT_PULSOS];

int calc_autocorrelacion();

float mega_matriz[2][2][CANT_GATE][CANT_PULSOS] = {0};

int main(int argc, char **argv)
{
	FILE *f;
	f = fopen(FILE_DIR, "rb");
	fseek(f, 0, SEEK_END);
	long int fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	unsigned char bin_file[fsize];

	fread(bin_file, fsize, 1, f);
	fseek(f, 0, SEEK_SET);
	fclose(f);

	unsigned short int validSample;

	int ii = 0, jj = 0, yy = 0, gg = 0;

	unsigned int pulsos = 0, cant_prom_gate = 0, len_gate = 0;

	float sampleV_I = 0, sampleV_Q = 0, sampleH_I = 0, sampleH_Q = 0, aux = 0;

	//#pragma omp parallel reduction(+ : sampleH_Q, sampleV_Q, sampleV_I, sampleV_Q)
	//{

	while (gg < sizeof(bin_file))
		{
			validSample = (bin_file[gg] ) + (bin_file[gg + 1] << 8) ;
			//printf("validSample %d\n", validSample);
			gg += sizeof(short int);

			cant_prom_gate = (float)validSample / CANT_GATE;

			yy = validSample - cant_prom_gate * CANT_GATE;

			if (yy == 0)
				len_gate = cant_prom_gate;
			else
				len_gate = cant_prom_gate + 1;

			for (ii = 0; ii < CANT_GATE; ii ++)
				{
					if ( ii >= yy )
						len_gate = cant_prom_gate;

					for (jj = 0; jj < len_gate; jj++)
						{
							memmove(&aux, &bin_file[gg], sizeof(float));
							//	#pragma omp critical
							//	{
							sampleV_I += aux;
							//	}
							gg += sizeof(float);

							memmove(&aux, &bin_file[gg], sizeof(float));
							//	#pragma omp critical
							//	{
							sampleV_Q += aux;
							//	}
							gg += sizeof(float);
						}

					mega_matriz[CANAL_V][COMP_I][ii][pulsos] = sampleV_I / len_gate;
					mega_matriz[CANAL_V][COMP_Q][ii][pulsos] = sampleV_Q / len_gate;
					//printf("mega_matriz[CANAL_V][COMP_I][%d][%d]: %f\n", ii, pulsos, mega_matriz[CANAL_V][COMP_I][ii][pulsos]);
					//printf("mega_matriz[CANAL_V][COMP_Q][%d][%d]: %f\n", ii, pulsos, mega_matriz[CANAL_V][COMP_Q][ii][pulsos]);
					sampleV_I = 0;
					sampleV_Q = 0;
				}

			if (yy == 0)
				len_gate = cant_prom_gate;
			else
				len_gate = cant_prom_gate + 1;

			for (ii = 0; ii < CANT_GATE; ii ++)
				{
					if ( ii >= yy )
						len_gate = cant_prom_gate;

					//#pragma omp parallel for reduction ( + : sampleH_I, sampleH_Q)
					for (jj = 0; jj < len_gate; jj++)
						{
							memmove(&aux, &bin_file[gg], sizeof(float));
							//#pragma omp critical
							//{
							sampleH_I += aux;
							//}
							gg += sizeof(float);

							memmove(&aux, &bin_file[gg], sizeof(float));
							//#pragma omp critical
							//{
							sampleH_Q += aux;
							//}
							gg += sizeof(float);
						}

					mega_matriz[CANAL_H][COMP_I][ii][pulsos] = sampleH_I / len_gate;
					mega_matriz[CANAL_H][COMP_Q][ii][pulsos] = sampleH_Q / len_gate;
					//printf("mega_matriz[CANAL_H][COMP_I][%d][%d]: %f\n", ii, pulsos, mega_matriz[CANAL_H][COMP_I][ii][pulsos]);
					//printf("mega_matriz[CANAL_H][COMP_Q][%d][%d]: %f\n", ii, pulsos, mega_matriz[CANAL_H][COMP_Q][ii][pulsos]);
					sampleH_I = 0;
					sampleH_Q = 0;
				}
			pulsos++;
			//printf("pulso: %d\n", pulsos);
		}
//}

	calc_autocorrelacion(&mega_matriz);

	return 0;
}

int calc_autocorrelacion()
{
	float matriz_autocorrelacion[2][CANT_GATE] = {0};
	float val_abs_m0 = 0, val_abs_m1 = 0;

	for (int i = 0; i < CANT_GATE; i++)
		{
			//printf("Thread number %d\n", omp_get_thread_num() );
			//#pragma omp parallel for reduction (+:matriz_autocorrelacion[:CANAL_V][:i], matriz_autocorrelacion[:CANAL_H][:i])
			for (int j = 0; j < CANT_PULSOS - 1; j++)
				{
					val_abs_m0 = sqrt(
					                 pow(mega_matriz[CANAL_V][COMP_I][i][j], 2) +
					                 pow(mega_matriz[CANAL_V][COMP_Q][i][j], 2));
					val_abs_m1 = sqrt(
					                 pow(mega_matriz[CANAL_V][COMP_I][i][j + 1], 2) +
					                 pow(mega_matriz[CANAL_V][COMP_Q][i][j + 1], 2));
					matriz_autocorrelacion[CANAL_V][i] += val_abs_m0 * val_abs_m1;

					val_abs_m0 = sqrt(
					                 pow(mega_matriz[CANAL_H][COMP_I][i][j], 2) +
					                 pow(mega_matriz[CANAL_H][COMP_Q][i][j], 2));
					val_abs_m1 = sqrt(
					                 pow(mega_matriz[CANAL_H][COMP_I][i][j + 1], 2) +
					                 pow(mega_matriz[CANAL_H][COMP_Q][i][j + 1], 2));
					matriz_autocorrelacion[CANAL_H][i] += val_abs_m0 * val_abs_m1;
				}

			matriz_autocorrelacion[CANAL_V][i] = matriz_autocorrelacion[CANAL_V][i] / CANT_PULSOS;
			matriz_autocorrelacion[CANAL_H][i] = matriz_autocorrelacion[CANAL_H][i] / CANT_PULSOS;
			printf("matriz_autocorrelacion[CANAL_V][%d]= %f\n", i, matriz_autocorrelacion[CANAL_V][i]);
			printf("matriz_autocorrelacion[CANAL_H][%d]= %f\n", i, matriz_autocorrelacion[CANAL_H][i]);
		}

	return 0;
}
