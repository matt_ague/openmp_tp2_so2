// gcc -o programa programa.c -fopenmp
#include <omp.h>
#include <stdio.h>


int main()
{
	int acum = 0;

	#pragma omp parallel num_threads(20)
	{
		#pragma omp critical
		{
			printf("Holiiis %d acum= %d\n", omp_get_thread_num(), acum);
			acum += 1;
		}
	}

	#pragma omp parallel num_threads(20)
	{

	}


	return 0;
}
/*
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#define SEED 35791246

int main(int argc, char* argv)
{
   int niter=0;
   double x,y;
   int i,count=0; // # of points in the 1st quadrant of unit circle
   double z;
   double pi;

   printf("Enter the number of iterations used to estimate pi: ");
   scanf("%d",&niter);

   // initialize random numbers 
   srand(SEED);
   count=0;
   #pragma omp parallel for reduction (+ : count)
   {
	   for ( i=0; i<niter; i++)
	   {
	      x = (double)rand()/RAND_MAX;
	      y = (double)rand()/RAND_MAX;
	      z = x*x+y*y;
	      if (z<=1) count++;
	   }
   }

   pi=((double)count/niter)*4;
   printf("# of trials= %d , estimate of pi is %f \n",niter, pi);
}
*/